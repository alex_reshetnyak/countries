import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ItemService } from '../item.service'
import { NgForm } from '@angular/forms'
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent implements OnInit, OnDestroy {

  @ViewChild('search') search: ElementRef;

  constructor(private itemService: ItemService) { }

  private countries: any;
  private answer: {country: string, population: number} = {
    country: "",
    population: 0
  };
  private hoveredItem: number = 0;
  private httpSubscription: Subscription;
  private timer;

  public ngOnInit() {
  	
  }

  public ngOnDestroy() {
    this.httpSubscription.unsubscribe();
  }

  public onSubmit(form: NgForm) {
    this.refreshAnswer(this.hoveredItem);
  }

  public onInput(event) {

    if (this.timer) {
      clearTimeout(this.timer);
    }
    const onComplete = () => {
      this.httpSubscription = this.itemService.search(event.target.value)
      .subscribe(resp => {
        this.countries = resp;
      });
    }
    this.timer = setTimeout(onComplete, 250);
  }


  public countryClick(index) {
    this.refreshAnswer(index);
  }

  public refreshAnswer(index: number) {
    this.answer.country = this.countries[index].locale_names[0];
    this.answer.population = this.countries[index].population;
    this.clearAll();
  }

  public clearAll() {
    this.countries = [];
    this.search.nativeElement.value = "";
    this.hoveredItem = null;
  }

  public checkESCTap(event) {
    console.log(event);
    if(event.keyCode === 27) {
      this.clearAll();
    }
  }

  public onMouseOut() {  // Clear hovered index 
   this.hoveredItem = 0;
 }

 public onMouseOver(index) { // Save index of hovered item
   this.hoveredItem = index;
 } 

}


//Дробдаун, первый всегда по умолчания подсвечен, после ентера дробдаун скрываеться, и выводиться его населения
//При наводе курсора по клику и энтеру выбераеться то на что наведено, по эскейпу всё убираеться (дробдаун и то что введено)