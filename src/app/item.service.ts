import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class ItemService {


	constructor(private http: HttpClient) {}

	public search(country: string): Observable<any> {
		return this.http.post<any>('https://places-dsn.algolia.net/1/places/query', {"query": country, "type": "country", "language": "en", "hitsPerPage": 5})
		.pipe(catchError(this.handleError))
		.pipe(map((response: any) => {
			return response.hits;
		}));
	}


	private handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			console.log(error.status);
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${error.status}, ` +
				`body was: ${error.error}`);
		}
		// return an observable with a user-facing error message
		return throwError(
			'Something bad happened; please try again later.'
			);
	};

}
